# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include mypaint
class mypaint (
  String        $package_ensure,
  Array[String] $package_name,
) {
  contain 'mypaint::install'
}
