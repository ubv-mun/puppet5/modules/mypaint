# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include mypaint::install
class mypaint::install {
  package { $mypaint::package_name:
    ensure => $mypaint::package_ensure,
  }
}
